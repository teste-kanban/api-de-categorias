# API de Categorias

Documentação da api de categorias.

# Método GET

GET catalog/categories.

# Método POST

POST catalog/categories.

# Método UPDATE

UPDTAE catalog/categories.

# Método PUT

PUT catalog/categories.

# PAYLOAD

```json
{
        "id": "spdfkebdrut0m34t",
        "name": "Modelo de grief",
        "tags": ["id1", "id2"],
        "parent": "spdfkebdrut0m36t",
        "path_from_root": [
            {
                "id": "k65a42220wq",
                "name": "Calçados"
            },
            {
                "id": "oawud8932739pv2",
                "name": "Sapato social"
            },
            {
                "id": "spdfkebdrut0m34t",
                "name": "Modelo de grief"
            }
        ]
    }
```


